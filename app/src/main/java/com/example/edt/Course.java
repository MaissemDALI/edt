package com.example.edt;

import android.os.Parcel;
import android.os.Parcelable;

public class Course implements Parcelable{
    public static final String TAG = Course.class.getSimpleName();

    public String ue;
    public int hstart;
    public int hend;
    public String teacher;
    public String group;
    public String location;
    public int date;
    public String type;

    public Course() { }

    public Course(int date,String ue, int hstart, int hend, String location, String teacher, String group,String type) {
        this.ue = ue;
        this.hstart = hstart;
        this.hend = hend;
        this.location = location;
        this.teacher = teacher;
        this.group = group;
        this.date = date;
        this.type = type;
    }

    protected Course(Parcel in) {
        ue = in.readString();
        hstart = in.readInt();
        hend = in.readInt();
        location = in.readString();
        teacher = in.readString();
        group = in.readString();
        date = in.readInt();
        type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ue);
        dest.writeInt(hstart);
        dest.writeInt(hend);
        dest.writeString(location);
        dest.writeString(teacher);
        dest.writeString(group);
        dest.writeInt(date);
        dest.writeString(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Course> CREATOR = new Creator<Course>() {
        @Override
        public Course createFromParcel(Parcel in) {
            return new Course(in);
        }

        @Override
        public Course[] newArray(int size) {
            return new Course[size];
        }
    };
}


package com.example.edt;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NoSharedPrefHomeActivity extends Activity {

    public static final String PREFS = "Promo&Group";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bis);
        final EditText prefcurriculum = findViewById(R.id.curriculuminput);
        final EditText prefclass = findViewById(R.id.classinput);
        final EditText prefgroup = findViewById(R.id.groupinput);
        SharedPreferences settings = getSharedPreferences(PREFS, 0);

        prefcurriculum.setText(settings.getString("curriculum",""));
        prefclass.setText(settings.getString("class",""));
        prefgroup.setText(settings.getString("group",""));

        Button confirm = findViewById(R.id.confirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String scurriculum = prefcurriculum.getText().toString();
                String sclass = prefclass.getText().toString();
                String sgroup = prefgroup.getText().toString();
                Log.d("app", "no shared group "+scurriculum+" "+sclass+" "+sgroup);
                if(!scurriculum.equals("") && !sclass.equals("") && !sgroup.equals("")) {
                    if(getIntent().getExtras().get("homestate").equals("finish"))
                    {
                        Log.d("app","finish");
                        SharedPreferences.Editor editor = getSharedPreferences(PREFS, 0).edit();
                        editor.putString("curriculum", scurriculum);
                        editor.putString("class", sclass);
                        editor.putString("group", sgroup);
                        editor.apply();
                        Intent intent =  new Intent(NoSharedPrefHomeActivity.this,HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else if(getIntent().getExtras().get("homestate").equals("unfinish"))
                    {
                        Log.d("app","unfinish");
                        SharedPreferences.Editor editor = getSharedPreferences(PREFS, 0).edit();
                        editor.putString("curriculum", scurriculum);
                        editor.putString("class", sclass);
                        editor.putString("group", sgroup);
                        editor.apply();
                        setResult(HomeActivity.RESULT_OK, new Intent());
                        finish();
                    }
                }
                else
                {
                    Log.d("app","empty");
                }
            }
        });





    }
}

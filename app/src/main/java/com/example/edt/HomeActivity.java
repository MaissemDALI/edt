package com.example.edt;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.net.MalformedURLException;

public class HomeActivity extends AppCompatActivity {

    public static final String PREFS = "Promo&Group";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bis);

        setSupportActionBar((Toolbar)findViewById(R.id.toolbar));


        new UpdateCalendar().execute();


        SharedPreferences settings = getSharedPreferences(PREFS, 0);
        //settings.edit().clear().apply();
        String prefcurriculum = settings.getString("curriculum",null);
        String prefclass = settings.getString("class",null);
        String prefgroup = settings.getString("group",null);
        if(prefcurriculum == null || prefclass == null || prefgroup == null){
            Intent intent =  new Intent(HomeActivity.this,NoSharedPrefHomeActivity.class);
            intent.putExtra("homestate","finish");
            startActivity(intent);
            finish();
        }
        getSupportActionBar().setTitle(prefcurriculum+" "+prefclass+" "+prefgroup);
        Log.d("app", "main group "+prefcurriculum+" "+prefclass+" "+prefgroup);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.home) {


            return true;
        }
        else if(id == R.id.schedule)
        {
            SharedPreferences settings = getSharedPreferences(PREFS, 0);
            settings.edit().clear().apply();
            return true;
        }
        else if(id == R.id.evaluation)
        {
            return true;
        }
        else if(id == R.id.free_room)
        {
            return true;
        }
        else if(id == R.id.option)
        {
            Intent intent =  new Intent(HomeActivity.this,NoSharedPrefHomeActivity.class);
            intent.putExtra("homestate","unfinish");
            startActivityForResult(intent,1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == HomeActivity.RESULT_OK) {

                SharedPreferences settings = getSharedPreferences(PREFS, 0);
                String prefcurriculum = settings.getString("curriculum", null);
                String prefclass = settings.getString("class", null);
                String prefgroup = settings.getString("group", null);
                getSupportActionBar().setTitle(prefcurriculum + " " + prefclass + " " + prefgroup);
            }
        }
    }
}

package com.example.edt;

import android.net.Uri;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {

    private static final String HOST = "edt-api.univ-avignon.fr";
    private static final String Route_1 = "app.php";
    private static final String Route_2 = "api";
    private static final String Route_3 = "exportAgenda";
    private static final String Route_4 = "diplome";
    private static final String Route_5 = "2-L3IN";

    public static URL build() throws MalformedURLException {

        String group = "2-L3IN";


        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority(HOST)
                .appendPath(Route_1)
                .appendPath(Route_2)
                .appendPath(Route_3)
                .appendPath(Route_4)
                .appendPath(Route_5);
        Log.d("app",builder.build().toString());
        URL url = new URL(builder.build().toString());
        return url;
    }
}

package com.example.edt;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ApiReader
{
    public ApiReader()
    {

    }

    public void read(InputStream in)
    {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(in,"UTF-8"));
            String line = br .readLine();
            while(line != null)
            {
                if(line.equals("BEGIN:VEVENT"))
                {
                    while(!line.equals("END:VEVENT"))
                    {
                        int sdate = 0;
                        int sheure = 0;
                        int findate = 0;
                        int finheure = 0;
                        String ue = "";
                        String prof = "";
                        String groupe = "";
                        String location = "";
                        String type = "";

                        line = br.readLine();
                        if(line.contains("DTSTART:"))
                        {
                            String[] cds = line.substring(8).split("T");
                            sdate = Integer.getInteger(cds[0]);
                            sheure = Integer.getInteger(cds[1].replace("Z",""));
                        }
                        else if(line.contains("DTEND:"))
                        {
                            String[] cde = line.substring(6).split("T");
                            findate = Integer.getInteger(cde[0]);
                            finheure = Integer.getInteger(cde[1].replace("Z",""));
                        }
                        else if(line.contains("DESCRIPTION;") && !line.contains("Annulation :")) {
                            //Log.d("read",line.substring(24));
                            String desc = line.substring(24);
                            desc.replace("\n","_");
                            String[] desctab = desc.split("_");
                            for (int i = 0;i<desctab.length;i++)
                            {
                                if(desctab[i].contains("Matière :"))
                                {
                                    ue = desctab[i].substring(10);
                                }
                                else if(desctab[i].contains("Enseignant :"))
                                {
                                    prof = desctab[i].substring(13);
                                }
                                else if(desctab[i].contains("Promotion :"))
                                {
                                    groupe = desctab[i].substring(12);
                                }
                                else if(desctab[i].contains("Salle :"))
                                {
                                    location = desctab[i].substring(8);
                                }
                                else if(desctab[i].contains("Type :"))
                                {
                                    type = desctab[i].substring(7);
                                }
                            }
                        }
                        Course tmp = new Course(sdate,ue,sheure,finheure,location,prof,groupe,type);
                    }
                }
                line = br.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

package com.example.edt;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class UpdateCalendar extends AsyncTask<Void,Void,String>{

    public UpdateCalendar() {

    }

    @Override
    protected String doInBackground(Void... v) {

        URL url = null;
        try {
            url = WebServiceUrl.build();
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            new ApiReader().read(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
